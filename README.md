# IRCJ (Java IRC Wrapper)
IRCJ is an IRC wrapper written in Java. It provides a wrapper which is still maintained and supported, unlike many other Java wrappers which have been abandoned or are no longer actively developed. Although the IRC protocol rarely changes, active development and optimisation should continue. I am to provide a fully functional and efficient library.

## Maven
Replace `VERSION` with the latest version (see badge at top of repo)
```xml
<repositories>
    <repository>
        <id>polarian-repo</id>
        <url>https://repo.polarian.dev</url>
    </repository>
</repositories>
```

```xml
<dependencies>
    <dependency>
        <groudId>dev.polarian</groudId>
        <artifactId>ircj</artifactId>
        <version>VERSION</version>
    </dependency>
</dependencies>
```
## Gradle
Replace `VERSION` with the latest version (see badge at top of repo)
```gradle
repositories {
    maven {
        url 'https://repo.polarian.dev'
    }
}
```

```gradle
dependencies {
    implementation 'dev.polarian:ircj:VERSION'
}
```

## Issues/Bugs
If you encounter any issues or bugs (or would like to provide suggestions), please open an issue and I will be happy to help,
or email me at `polarian@polarian.dev`

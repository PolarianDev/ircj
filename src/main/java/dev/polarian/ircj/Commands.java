package dev.polarian.ircj;

import java.io.IOException;

/**
 * Class containing all commands which can interact with the IRC server
 */
public class Commands {

    /**
     * The IRC client in which the command will be sent to
     */
    IrcClient client;

    public Commands(IrcClient client) {
        this.client = client;
    }

    /**
     * Join an IRC channel
     *
     * @param channel The channel to join in the format: #channel, replacing channel with the channel name
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void joinChannel(String channel) throws IOException {
        client.sendCommand("JOIN " + channel);
    }

    /**
     * Part an IRC channel
     *
     * @param channel The channel to part in the format: #channel, replacing channel with the channel name
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void partChannel(String channel) throws IOException {
        client.sendCommand("PART " + channel);
    }

    /**
     * Part an IRC channel with a message
     *
     * @param channel The channel to part in the format: #channel, replacing the channel with the channel name
     * @param message The message sent to other users when you part the channel
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void partChannel(String channel, String message) throws IOException {
        client.sendCommand(String.format("PART %s :%s", channel, message));
    }

    /**
     * Send a message to an IRC channel
     *
     * @param channel The channel to send the message to
     * @param message The message you would like to send
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void sendMessage(String channel, String message) throws IOException {
        client.sendCommand(String.format("PRIVMSG %s :%s", channel, message));
    }

    /**
     * Sends mode to the IRC server for the user
     *
     * @param username The username of the bot
     * @param mode     The modes which are sent to the server
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void setMode(String username, String mode) throws IOException {
        client.sendCommand(String.format("MODE %s %s", username, mode));
    }

    /**
     * Sends nickname to the IRC server
     *
     * @param nick The desired nickname
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void setNick(String nick) throws IOException {
        client.sendCommand("NICK " + nick);
    }

    /**
     * Send the user credentials to the server, used for authorisation when connection is first established
     * with the IRC daemon
     *
     * @param username The desired username of the user
     * @param mode     The desired mode
     * @param realName Real name of the user
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void sendUser(String username, UserMode mode, String realName) throws IOException {
        client.sendCommand(String.format("USER %s %d * :%s", username, mode.getBit(), realName));
    }

    /**
     * Set the topic of a channel
     *
     * @param channel The channel to set the topic of
     * @param topic   The topic to be set
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void setTopic(String channel, String topic) throws IOException {
        client.sendCommand(String.format("TOPIC %s :%s", channel, topic));
    }

    /**
     * Disconnect from IRC daemon with sending a message to the channels in which the client has joined
     *
     * @param message Disconnect message sent to the channels which the client has joined
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void disconnect(String message) throws IOException {
        client.sendCommand("QUIT :" + message);
        client.setForceDisconnected(true);
    }

    /**
     * Disconnect from IRC daemon without sending a message
     *
     * @throws IOException if an exception is thrown while attempting to write to OutputStream
     */
    public void disconnect() throws IOException {
        client.sendCommand("QUIT");
        client.setForceDisconnected(true);
    }

    /**
     * Get the IrcClient which created the object
     *
     * @return A reference to the IrcClient which created the object
     */
    public IrcClient getClient() {
        return this.client;
    }
}

package dev.polarian.ircj.utils;

/**
 * Enum containing all the possible formatting for IRC (which the majority of IRC clients support)
 */
public enum Formatting {
    BOLD("\u0002"),
    ITALICS("\u001D"),
    UNDERLINE("\u001F"),
    STRIKETHROUGH("\u001E"),
    MONOSPACE("\u0011"),
    COLOUR_WHITE("\u00030"),
    COLOUR_BLACK("\u00031"),
    COLOUR_BLUE("\u00032"),
    COLOUR_GREEN("\u00033"),
    COLOUR_RED("\u00034"),
    COLOUR_BROWN("\u00035"),
    COLOUR_PURPLE("\u00036"),
    COLOUR_ORANGE("\u00037"),
    COLOUR_YELLOW("\u00038"),
    COLOUR_LIGHT_GREEN("\u00039"),
    COLOUR_CYAN("\u000310"),
    COLOUR_LIGHT_CYAN("\u000311"),
    COLOUR_LIGHT_BLUE("\u000312"),
    COLOUR_PINK("\u000313"),
    COLOUR_GRAY("\u000314"),
    COLOUR_LIGHT_GRAY("\u000315"),
    ZWSP("\u200B");
    private final String format;

    Formatting(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    @Override
    public String toString() {
        return format;
    }
}

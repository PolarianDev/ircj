package dev.polarian.ircj;

import dev.polarian.ircj.objects.events.WelcomeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class EventSubscriber extends EventListener {
    private static final Logger logger = LoggerFactory.getLogger(EventSubscriber.class);
    private final List<String> channels;

    public EventSubscriber(List<String> channels) {
        this.channels = channels;
    }

    @Override
    public void onWelcomeEvent(WelcomeEvent event) {
        logger.info("Successfully connected to IRC server");
        event.getClient().setConnected(true);
        for (String channel : channels) {
            try {
                event.joinChannel(channel);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        event.getClient().getLatch().countDown();
    }
}

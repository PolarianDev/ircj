package dev.polarian.ircj;

public enum DisconnectReason {
    ERROR,
    FORCE_DISCONNECTED,
    TIMEOUT
}

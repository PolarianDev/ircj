package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.Commands;
import dev.polarian.ircj.IrcClient;

/**
 * Base abstract class for any message, contains all the methods which every message would need, also contains
 * abstracted methods used to interact with IRC so that the IRC commands do not need to be manually entered.
 * This therefore means that any event can interact with the IRC server.
 */
public abstract class Message extends Commands {
    /**
     * The nickname of the user referenced in the message
     */
    String nick;

    /**
     * Constructor to instantiate a daughter class which extends this abstract class, used with super()
     *
     * @param nick   The nickname of the user
     * @param client The IRC client which creates the object
     */
    protected Message(String nick, IrcClient client) {
        super(client);
        this.nick = nick;
    }

    /**
     * Get the nickname of the user which is referenced within the message
     *
     * @return A String containing the nickname of the user referenced in the message
     */
    public String getNick() {
        return this.nick;
    }
}

package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

public class ChannelPartEvent extends CommandMessage {
    /**
     * public constructor to create an object
     *
     * @param client   The IrcClient which creates the object
     * @param nick     The nickname of the user
     * @param command  The command which invoked the event
     * @param channel  The channel referenced in the message
     * @param username The username of the user
     * @param userhost The IP or Hostname of the user
     */
    public ChannelPartEvent(IrcClient client, String nick, String command, String channel, String username, String userhost) {
        super(client, nick, command, channel, username, userhost);
    }
}

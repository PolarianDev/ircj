package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

/**
 * Abstract class which contains methods related to a message which contain response codes
 */
public abstract class CodeMessage extends Message {
    /**
     * The response code of the message
     */
    String code;

    /**
     * The name of the IRC server referenced in the message
     */
    String serverName;

    /**
     * Constructor to instantiate a daughter class which extends this abstract class, used with super()
     *
     * @param client     The IrcClient which creates the object
     * @param nick       The nickname of the user
     * @param code       The response code of the message
     * @param serverName The name of the IRC server referenced in the message
     */
    protected CodeMessage(IrcClient client, String nick, String code, String serverName) {
        super(nick, client);
        this.code = code;
        this.serverName = serverName;
    }

    /**
     * Gets the response code tied to the message
     *
     * @return The response code tied to the message
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Gets the server name which is tied to the message
     *
     * @return The server name tied to the message
     */
    public String getServerName() {
        return this.serverName;
    }
}

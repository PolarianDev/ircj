package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

/**
 * Abstract method containing methods related to messages which reference the User, this is mainly messages which
 * use Commands, messages with codes are formatted differently (see CodeMessage for more info).
 *
 * @see CodeMessage
 */
public abstract class CommandMessage extends Message {
    /**
     * The command which is tied to the specific message type
     */
    String command;

    /**
     * The channel referenced within the message
     */
    String channel;

    /**
     * The username of the user referenced in the message
     */
    String username;

    /**
     * The hostname or IP address of the user referenced in the message
     */
    String userhost;

    /**
     * Constructor to instantiate a daughter class which extends this abstract class, used with super()
     *
     * @param client   The IrcClient which creates the object
     * @param nick     The nickname of the user
     * @param command  The command which invoked the event
     * @param channel  The channel referenced in the message
     * @param username The username of the user
     * @param userhost The IP or Hostname of the user
     */
    protected CommandMessage(IrcClient client, String nick, String command, String channel, String username, String userhost) {
        super(nick, client);
        this.command = command;
        this.channel = channel;
        this.username = username;
        this.userhost = userhost;
    }

    /**
     * Set the connection info of the user, for example this could be the ip or the domain in which the user
     * in which the user has connected using.
     *
     * @return IP or hostname of the user as a String
     */
    public String getUserhost() {
        return userhost;
    }

    /**
     * Get the username of the user
     *
     * @return The username of the user as a String
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the channel referenced in the message
     *
     * @return The channel referenced in the message as a String
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Get the command which is tied to the message type, example command would be PRIVMSG
     *
     * @return String containing the command tied to the message type
     */
    public String getCommand() {
        return command;
    }
}

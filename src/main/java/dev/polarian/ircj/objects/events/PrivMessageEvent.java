package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

/**
 * Privmsg is received when a message is sent within a channel, whether the channel is public or private, message
 * contains the IRC message sent within channels.
 */
public class PrivMessageEvent extends CommandMessage {
    /**
     * The content of the message sent
     */
    String message;

    /**
     * Constructor to create a PrivMessage object
     *
     * @param client   The IrcClient which creates the object
     * @param nick     The nickname of the user
     * @param command  The command which invoked the event
     * @param channel  The channel referenced within the message
     * @param username The username of the user
     * @param userhost The IP or Hostname of the user
     * @param message  The content of the message sent by the user within the channel specified in the PrivMessage
     */
    public PrivMessageEvent(IrcClient client, String nick, String command, String channel, String username, String userhost, String message) {
        super(client, nick, command, channel, username, userhost);
        this.message = message;
    }

    /**
     * Gets the content of the message sent
     *
     * @return A string containing the content of the message
     */
    public String getMessage() {
        return message;
    }
}

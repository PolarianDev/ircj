package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

/**
 * Welcome message of the IRC server (code 001)
 */
public class WelcomeEvent extends CodeMessage {
    String greetingMessage;

    /**
     * Constructor to create a WelcomeMessage object. This should not be used outside the wrapper codebase.
     *
     * @param client          The IrcClient which creates the object
     * @param nick            The nickname of the user
     * @param code            The response code of the message
     * @param serverName      The name of the IRC server referenced in the message
     * @param greetingMessage The greeting message sent by the IRC server upon successful connection
     */
    public WelcomeEvent(IrcClient client, String nick, String code, String serverName, String greetingMessage) {
        super(client, nick, code, serverName);
        this.greetingMessage = greetingMessage;
    }

    /**
     * Gets the greeting message sent by the IRC server upon successful connection
     *
     * @return A String containing the greeting message
     */
    public String getGreetingMessage() {
        return this.greetingMessage;
    }

    /**
     * Sets the greeting message sent by the IRC server
     *
     * @param greetingMessage The greeting message sent by the IRC server
     * @return returns current instance so that it can be used for chaining
     */
    WelcomeEvent setGreetingMessage(String greetingMessage) {
        this.greetingMessage = greetingMessage;
        return this;
    }
}

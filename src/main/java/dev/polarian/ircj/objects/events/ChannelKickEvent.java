package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

public class ChannelKickEvent extends CommandMessage {
    /**
     * The user which was kicked from channel
     */
    String kickedUser;
    /**
     * True --> the kicked user was the current user
     * False --> the kicked user was another user
     */
    boolean isSelf;

    /**
     * Constructor to create a ChannelKickEvent
     *
     * @param client   The IrcClient which creates the object
     * @param nick     The nickname of the user
     * @param command  The command which invoked the event
     * @param channel  The channel referenced in the message
     * @param username The username of the user
     * @param userhost The IP or Hostname of the user
     */
    public ChannelKickEvent(IrcClient client, String nick, String command, String channel, String username, String userhost, String kickedUser) {
        super(client, nick, command, channel, username, userhost);
        this.kickedUser = kickedUser;
        this.isSelf = kickedUser.equalsIgnoreCase(client.getConfig().getNickname());
    }

    /**
     * Check if the kicked user was the client:
     * true --> the kicked user was the current user
     * false --> the kicked user was another user
     *
     * @return whether the current client was the kicked user
     */
    public boolean isSelf() {
        return isSelf;
    }

    /**
     * Get the nickname of the kicked user
     *
     * @return the username of the kicked user
     */
    public String getKickedUser() {
        return kickedUser;
    }
}

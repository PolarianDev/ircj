package dev.polarian.ircj.objects.events;

import dev.polarian.ircj.IrcClient;

/**
 * Join Message which is received when the IRC client joins a new channel
 */
public class ChannelJoinEvent extends CommandMessage {

    /**
     * Public constructor to create an object
     *
     * @param client   The IrcClient which creates the object
     * @param nick     The nickname of the user
     * @param command  The command which invoked the event
     * @param channel  The channel which is referenced within the message
     * @param username The username of the user
     * @param userhost The IP or Hostname of the user
     */
    public ChannelJoinEvent(IrcClient client, String nick, String command, String channel, String username, String userhost) {
        super(client, nick, command, channel, username, userhost);
    }
}

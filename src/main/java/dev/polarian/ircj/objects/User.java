package dev.polarian.ircj.objects;

/**
 * A class which encapsulates IRC User information
 */
public class User {
    /**
     * Nickname of the IRC user
     */
    String nick;

    /**
     * Username of the IRC user
     */
    String username;

    /**
     * Domain or IP address which is used to connect to the IRC server by the user
     */
    String host;

    /**
     * Constructor to create a User object
     *
     * @param nick     The nickname of the user
     * @param username The username of the user
     * @param host     The IP address or the domain which the user used to connect to the IRC server
     */
    public User(String nick, String username, String host) {
        this.nick = nick;
        this.username = username;
        this.host = host;
    }

    /**
     * Gets the domain or IP address the user used to connect to the IRC server
     *
     * @return The domain or Ip address used by the user to connect to the IRC server as a String
     */
    public String getHost() {
        return host;
    }

    /**
     * Gets the nickname of the user
     *
     * @return The nickname of the user as a String
     */
    public String getNick() {
        return nick;
    }

    /**
     * Gets the username of the user
     *
     * @return The username of the user as a String
     */
    public String getUsername() {
        return username;
    }
}

package dev.polarian.ircj.objects;

import dev.polarian.ircj.UserMode;

import java.util.List;

/**
 * A class which encapsulates the configuration for the IrcClient
 */
public class Config {
    private String hostname;
    private String nickname;
    private String username;
    private int port;
    private List<String> channels;
    private int timeout;
    private boolean tls = false;
    private UserMode userMode;
    private String realName;

    /**
     * Create a minimal IRC config, without tls encryption and by passing in a List of channels
     *
     * @param hostname The hostname or ip of the IRC server
     * @param port     The non-tls port of the IRC server
     * @param nickname The nickname of the user (will also be the username and real name)
     * @param channels A list of channels which the client will attempt to join on successful connection
     */
    public Config(String hostname, int port, String nickname, List<String> channels) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.tls = false;
        this.channels = channels;

        this.username = nickname;
        this.realName = nickname;
        this.timeout = 5000;
        this.userMode = UserMode.NONE;
    }

    /**
     * Create a minimal IRC config, without tls encryption and by passing in a single channel as a String
     *
     * @param hostname The hostname or ip of the IRC server
     * @param port     The non-tls port of the IRC server
     * @param nickname The nickname of the user (will also be the username and real name)
     * @param channel  A single IRC channel as a String
     */
    public Config(String hostname, int port, String nickname, String channel) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.channels = List.of(channel);

        this.tls = false;
        this.username = nickname;
        this.realName = nickname;
        this.timeout = 5000;
        this.userMode = UserMode.NONE;
    }

    /**
     * Create an IRC config with optional tls encryption, with a list of channels (multiple channels), and using the
     * nickname as the real name and the username
     *
     * @param hostname The hostname or the ip of the IRC server
     * @param port     The port of the IRC server
     * @param nickname The nickname of the user (will also be the username and real name)
     * @param channels A list of channels which the IRC client will join on connection
     * @param tls      Whether tls encryption will be used
     */
    public Config(String hostname, int port, String nickname, List<String> channels, boolean tls) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.channels = channels;
        this.tls = tls;

        this.username = nickname;
        this.realName = nickname;
        this.timeout = 5000;
        this.userMode = UserMode.NONE;
    }

    /**
     * Create an IRC config with optional tls encryption, with a channel as a String (single channel), and using
     * the nickname as the real name and the username
     * `
     *
     * @param hostname The hostname or ip of the IRC server
     * @param port     The port of the IRC server
     * @param nickname The nickname of the user (will also be the username and real name)
     * @param channel  A single channel as a String
     * @param tls      Whether tls encryption will be used
     */
    public Config(String hostname, int port, String nickname, String channel, boolean tls) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.channels = List.of(channel);
        this.tls = tls;

        this.username = nickname;
        this.realName = nickname;
        this.timeout = 5000;
        this.userMode = UserMode.NONE;
    }

    /**
     * Create an IRC config with optional TLS encryption, nickname, username, real name, default timeout of 5s
     * and a list of channels (multiple channels)
     *
     * @param hostname The hostname or ip of the IRC server
     * @param port     The IRC server port
     * @param nickname The nickname of the user
     * @param username The username of the user
     * @param realName The username real name
     * @param channels A list of channels the client will join when connected to the IRC server
     * @param tls      Whether tls encryption will be used
     */
    public Config(String hostname, int port, String nickname, String username, String realName, List<String> channels,
                  boolean tls) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.username = username;
        this.realName = realName;
        this.channels = channels;
        this.tls = tls;
        this.timeout = 5000;
        this.userMode = UserMode.NONE;
    }

    /**
     * Create an IRC config with optional TLS encryption, nickname, username real name, default timeout of 5s and
     * a channel as a String (single channel)
     *
     * @param hostname The hostname or ip of the IRC server
     * @param port     The IRC server port
     * @param nickname The nickname of the user
     * @param username The username of the user
     * @param realName The real name of the user
     * @param channel  The channel to join when the IRC client connects
     * @param tls      Whether tls encryption will be used
     */
    public Config(String hostname, int port, String nickname, String username, String realName, String channel,
                  boolean tls) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.username = username;
        this.realName = realName;
        this.channels = List.of(channel);
        this.tls = tls;
        this.timeout = 5000;
        this.userMode = UserMode.NONE;
    }

    /**
     * Create a full IRC Config, with no default values
     *
     * @param hostname The hostname or ip of the IRC server
     * @param port     The IRC server port
     * @param nickname The nickname of the user
     * @param username The username of the user
     * @param realName The real name of the user
     * @param channel  The channel to join when the IRC client connects to the IRC server (single channel)
     * @param tls      Whether tls encryption will be used
     * @param timeout  The time the client will attempt to join the IRC server before timing out (in milliseconds)
     * @param mode     The connection mode
     * @see UserMode
     */
    public Config(String hostname, int port, String nickname, String username, String realName, String channel,
                  boolean tls, int timeout, UserMode mode) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.username = username;
        this.realName = realName;
        this.channels = List.of(channel);
        this.tls = tls;
        this.timeout = timeout;
        this.userMode = mode;
    }

    /**
     * Create a full IRC Config, with no default values
     *
     * @param hostname The hostname or the ip of the IRC server
     * @param port     The IRC server port
     * @param nickname The nickname of the user
     * @param username The username of the user
     * @param realName The real name of the user
     * @param channels The channels to join when the IRC client connects to the IRC server (multiple channels)
     * @param tls      Whether tls encryption will be used
     * @param timeout  The time the client will attempt to join the IFC server before timing out (in milliseconds)
     * @param mode     The connection mode
     * @see UserMode
     */
    public Config(String hostname, int port, String nickname, String username, String realName, List<String> channels,
                  boolean tls, int timeout, UserMode mode) {
        this.hostname = hostname;
        this.port = port;
        this.nickname = nickname;
        this.username = username;
        this.realName = realName;
        this.channels = channels;
        this.tls = tls;
        this.timeout = timeout;
        this.userMode = mode;
    }


    /**
     * Get the hostname of the IRC server
     *
     * @return String containing the IP address or domain of the IRC server
     */
    public String getHostname() {
        return this.hostname;
    }

    /**
     * Set the hostname of the IRC server
     *
     * @param hostname A String containing either the domain or IP address of the IRC Server to connect to
     * @return Current instance to allow for setters to be chained together
     */
    public Config setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    /**
     * Get the nickname of the IRC User
     *
     * @return String containing the nickname of the IRC User
     */
    public String getNickname() {
        return this.nickname;
    }

    /**
     * Set the nickname for the IRC User
     *
     * @param nickname A String containing the nickname of the IRC User
     * @return Current instance to allow for setters to be chained together
     */
    public Config setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    /**
     * Get the username of the IRC User
     *
     * @return String containing the username of the IRC User
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Set the username for the IRC User
     *
     * @param username A String containing the username of the IRC User
     * @return Current instance to allow for setters to be chained together
     */
    public Config setUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     * Get the port of the IRC Server
     *
     * @return The port of the IRC server, as an int
     */
    public int getPort() {
        return this.port;
    }

    /**
     * Set the port of the IRC server
     *
     * @param port The port of the IRC server as an int
     * @return Current instance to allow setters to be chained together
     */
    public Config setPort(int port) {
        this.port = port;
        return this;
    }

    /**
     * Get the connection timeout
     *
     * @return The timeout for the connection, as an int in millis
     */
    public int getTimeout() {
        return this.timeout;
    }

    /**
     * Set the timeout for the attempt to connect to the IRC server
     *
     * @param timeout the time(in millis) that the IRC Client will attempt to connect to the IRC server
     * @return Current instance to allow setters to be chained together
     */
    public Config setTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    /**
     * Get the IRC channels in which the client will join upon successful connection
     *
     * @return The list of channels which the IRC client will join upon successful connection
     */
    public List<String> getChannels() {
        return this.channels;
    }

    /**
     * Set the channels the IRC Client will join upon successful connection
     *
     * @param channels A List of Strings containing the IRC channels the Client will join upon successful connection
     * @return Current instance to allow setters to be changed together
     */
    public Config setChannels(List<String> channels) {
        this.channels = channels;
        return this;
    }

    /**
     * Check whether SSL/TLS encryption is enabled
     *
     * @return true: SSL/TLS encryption enabled, false: SSL/TLS encryption disabled
     */
    public boolean isTls() {
        return this.tls;
    }

    /**
     * Set whether SSL/TLS encryption should be used to connect to the IRC server, DEFAULT: false
     *
     * @param tls true: SSL/TLS encryption enabled, false: SSL/TLS encryption disabled
     * @return Current instance to allow setters to be chained together
     */
    public Config setTls(boolean tls) {
        this.tls = tls;
        return this;
    }

    /**
     * Get the UserMode
     *
     * @return The UserMode to request
     */
    public UserMode getUserMode() {
        return this.userMode;
    }

    /**
     * Set the mode sent in the USER command to authenticate with the IRC daemon, set UserMode.NONE if none is required
     *
     * @param userMode The mode desired
     * @return Current instance to allow setter to be chained together
     */
    public Config setUserMode(UserMode userMode) {
        this.userMode = userMode;
        return this;
    }

    /**
     * Get the real name of the user
     *
     * @return The real name of the user
     */
    public String getRealName() {
        return this.realName;
    }

    /**
     * Set the real name of the user
     *
     * @param realName The real name of the user
     * @return Current instance to allow setter to be chained together
     */
    public Config setRealName(String realName) {
        this.realName = realName;
        return this;
    }
}

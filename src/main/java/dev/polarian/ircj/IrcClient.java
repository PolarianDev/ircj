package dev.polarian.ircj;

import dev.polarian.ircj.objects.Config;
import dev.polarian.ircj.objects.User;
import dev.polarian.ircj.objects.events.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * A client which can connected to a single IRC server, if multiple connections are required, create multiple IrcClients
 *
 * @author PolarianDev
 */
public class IrcClient {

    /**
     * Countdown latch to await all channels defined in the config object to have been joined
     */
    private final CountDownLatch latch;
    /**
     * A list of all the event listeners
     */
    private final List<EventListener> listeners = new ArrayList<>();
    /**
     * An object used to send commands to the IRC daemon
     */
    private final Commands command = new Commands(this);
    /**
     * Configuration object, contains all the configuration options available for the IrcClient
     */
    private final Config config;
    /**
     * Buffered reader which reads the InputStream handled by the socket
     */
    private BufferedReader rx;
    /**
     * Buffered writer which writes to the OutputStream handled by the socket
     */
    private BufferedWriter tx;
    /**
     * Whether the IRC Client is currently connected to the IRC server
     */
    private boolean connected = false;
    /**
     * Whether the IRC Client was forced to disconnect or disconnected due to error
     */
    private boolean forceDisconnected = false;
    /**
     * Thread which the client runs on
     */
    private Thread ircThread;

    /**
     * IRC client constructor, initialises default subscribers which provide abstracted features such as automatic
     * channel joining upon connecting to the IRC server.
     *
     * @param config Config object containing the configuration for the IrcClient
     * @see Config
     */
    public IrcClient(Config config) {
        this.config = config;
        this.addListener(new EventSubscriber(config.getChannels()));
        this.latch = new CountDownLatch(1);
    }

    /**
     * Checks whether the IRC client is connected
     *
     * @return true: connected, false: disconnected
     */
    public boolean isConnected() {
        return this.connected;
    }

    /**
     * Set whether the client is connected or not
     *
     * @param connected true --> client connected, false --> client disconnected
     */
    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    /**
     * Await for the client to have completely connected
     *
     * @throws InterruptedException If an interrupt occurs during await
     */
    public void awaitReady() throws InterruptedException {
        latch.await();
    }

    /**
     * Get the Countdown latch
     *
     * @return The client's countdown latch
     */
    public CountDownLatch getLatch() {
        return latch;
    }

    /**
     * Check whether the client was forcefully disconnected
     *
     * @return true --> force disconnected, false --> disconnected by other means or still connected
     */
    boolean isForceDisconnected() {
        return forceDisconnected;
    }

    /**
     * Set whether the client was forcefully disconnected using the disconnect() method
     *
     * @param forceDisconnected true --> force disconnected, false --> not force disconnected/still connected
     */
    void setForceDisconnected(boolean forceDisconnected) {
        this.forceDisconnected = forceDisconnected;
    }

    /**
     * Gets the IrcClient Config object, which contains the configuration for the IrcClient
     *
     * @return The IrcClient Config object, containing the client configuration
     */
    public Config getConfig() {
        return this.config;
    }

    /**
     * Get the commands object allowing you to send commands to the IRC daemon
     *
     * @return commands instance
     */
    public Commands getCommands() {
        return command;
    }

    /**
     * Method which appends an instance of an event listener to the listeners
     *
     * @param listener Instance of the event listener which extends EventListener
     * @see EventListener Extend this class in order to handle events
     */
    public void addListener(EventListener listener) {
        listeners.add(listener);
    }

    /**
     * parses the long user string sent by the IRC server which contains the nickname, username and the connection info
     *
     * @param user User String which is sent by the IRC server
     * @return returns a User object containing the user info
     */
    private User parseUser(String user) {
        int i = user.indexOf("!");
        int i2 = user.indexOf("@");
        return new User(user.substring(0, i), user.substring(i + 2, i2), user.substring(i2));
    }

    /**
     * Parse and handle the message received from the socket
     *
     * @param msg The message received from the InputStream
     */
    void parseMessage(String msg) {
        int i = msg.indexOf(':', 1);
        if (i == -1) {
            return;
        }
        String[] args = msg.substring(1, i).split(" ");
        User user;
        switch (args[1]) {
            case "001":
                connected = true;
                WelcomeEvent welcomeEvent = new WelcomeEvent(
                        this,
                        args[2],
                        "001",
                        args[0],
                        msg.substring(i)
                );
                for (EventListener listener : listeners) {
                    listener.onWelcomeEvent(welcomeEvent);
                }
                break;

            case "JOIN":
                user = parseUser(args[0]);
                ChannelJoinEvent channelJoinEvent = new ChannelJoinEvent(
                        this,
                        user.getNick(),
                        "JOIN",
                        msg.substring(i + 1),
                        user.getUsername(),
                        user.getHost()
                );
                for (EventListener listener : listeners) {
                    listener.onChannelJoinEvent(channelJoinEvent);
                }
                break;

            case "PRIVMSG":
                user = parseUser(args[0]);
                PrivMessageEvent privMessageEvent = new PrivMessageEvent(
                        this,
                        user.getNick(),
                        "PRIVMSG",
                        args[2],
                        user.getUsername(),
                        user.getHost(),
                        msg.substring(i + 1)
                );
                for (EventListener listener : listeners) {
                    listener.onPrivMessageEvent(privMessageEvent);
                }
                break;

            case "PART":
                user = parseUser(args[0]);
                ChannelPartEvent channelPartEvent = new ChannelPartEvent(
                        this,
                        user.getNick(),
                        "PART",
                        args[2],
                        user.getUsername(),
                        user.getHost()
                );
                for (EventListener listener : listeners) {
                    listener.onChannelPartEvent(channelPartEvent);
                }
                break;

            case "TOPIC":
                user = parseUser(args[0]);
                TopicChangeEvent topicChangeEvent = new TopicChangeEvent(
                        this,
                        user.getNick(),
                        "TOPIC",
                        args[2],
                        user.getUsername(),
                        user.getHost(),
                        msg.substring(i)
                );
                for (EventListener listener : listeners) {
                    listener.onTopicChangeEvent(topicChangeEvent);
                }
                break;

            case "KICK":
                user = parseUser(args[0]);
                ChannelKickEvent channelKickEvent = new ChannelKickEvent(
                        this,
                        user.getNick(),
                        "KICK",
                        args[2],
                        user.getUsername(),
                        user.getHost(),
                        args[3]
                );
                for (EventListener listener : listeners) {
                    listener.onChannelKickEvent(channelKickEvent);
                }
        }
    }

    /**
     * Send a command to the IRC server
     *
     * @param command A String containing the command
     * @throws IOException if an exception was thrown writing to OutputStream
     */
    public synchronized void sendCommand(String command) throws IOException {
        if (tx == null) return;
        tx.write(command + "\r\n");
        tx.flush();
    }

    public List<EventListener> getEventListeners() {
        return listeners;
    }

    /**
     * Returns the BufferedReader which reads the Socket Stream
     *
     * @return BufferedReader which reads the Socket Stream
     */
    BufferedReader getReader() {
        return rx;
    }

    /**
     * Sets the BufferedReader which reads the Socket Stream
     *
     * @param rx The BufferedReader
     */
    void setReader(BufferedReader rx) {
        this.rx = rx;
    }

    /**
     * Returns BufferedWriter which writes to the Socket Stream
     *
     * @return BufferedWriter which writes to the socket stream
     */
    BufferedWriter getWriter() {
        return tx;
    }

    /**
     * Sets the BufferedWriter which writes to the Socket Stream
     *
     * @param tx The BufferedWriter
     */
    void setWriter(BufferedWriter tx) {
        this.tx = tx;
    }

    Thread getThread() {
        return ircThread;
    }

    /**
     * Connect to the IRC server which is defined in the Config object passed when creating the instance.
     * This will block the thread it is executed on, if you are using the main thread for another task,
     * create a new thread to execute it on.
     *
     * @see Thread
     */
    public void connect() {
        if (ircThread != null) return;
        ircThread = new IrcThread(this);
        ircThread.start();
    }
}

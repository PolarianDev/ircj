package dev.polarian.ircj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class IrcThread extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(IrcThread.class);
    private final IrcClient client;

    public IrcThread(IrcClient client) {
        this.setName("IRC-Thread");
        this.client = client;
    }

    @Override
    public void run() {
        try {
            logger.info("Attempting to connect to IRC server...");
            // Creates the socket to connect to the IRC server
            Socket socket;
            if (client.getConfig().isTls()) {
                socket = SSLSocketFactory.getDefault().createSocket();
            } else {
                socket = SocketFactory.getDefault().createSocket();
            }

            // Create connection
            InetSocketAddress address = new InetSocketAddress(client.getConfig().getHostname(), client.getConfig().getPort());
            try {
                socket.connect(address, client.getConfig().getTimeout());
            } catch (SocketTimeoutException e) {
                client.getLatch().countDown();
            }
            client.setReader(new BufferedReader(new InputStreamReader(socket.getInputStream())));
            client.setWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
            logger.info("Connection established...");

            // Send credentials
            logger.info("Sending credentials...");
            client.getCommands().sendUser(client.getConfig().getUsername(),
                    client.getConfig().getUserMode(),
                    client.getConfig().getRealName());
            client.getCommands().setNick(client.getConfig().getNickname());

            String message;
            BufferedReader rx = client.getReader();
            while ((message = rx.readLine()) != null) {
                logger.debug(message);
                if (message.startsWith("PING")) {
                    client.sendCommand("PONG " + message.substring(5));
                    continue;
                }
                if (message.startsWith("ERROR")) {
                    continue;
                }
                client.parseMessage(message);
            }
            client.setConnected(false);
            logger.warn("Disconnected from IRC server");
            DisconnectReason reason;
            if (client.isForceDisconnected()) {
                reason = DisconnectReason.FORCE_DISCONNECTED;
            } else {
                reason = DisconnectReason.TIMEOUT;
            }
            for (EventListener listener : client.getEventListeners()) {
                listener.onDisconnectEvent(reason, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (client.isConnected()) {
                try {
                    client.getCommands().disconnect("An error has occurred");
                } catch (IOException ex) {
                    logger.error("Failed to disconnect from IRC server");
                    ex.printStackTrace();
                }
                for (EventListener listener : client.getEventListeners()) {
                    listener.onDisconnectEvent(DisconnectReason.ERROR, e);
                }
            }
        }
    }
}

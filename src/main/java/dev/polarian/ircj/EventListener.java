package dev.polarian.ircj;

import dev.polarian.ircj.objects.events.*;

/**
 * Contains all the invokable events, extend this class and Override the methods to handle the events
 */
public class EventListener {

    public void onChannelJoinEvent(ChannelJoinEvent event) {
    }

    public void onChannelPartEvent(ChannelPartEvent event) {
    }

    public void onDisconnectEvent(DisconnectReason reason, Exception e) {
    }

    public void onPrivMessageEvent(PrivMessageEvent event) {
    }

    public void onWelcomeEvent(WelcomeEvent event) {
    }

    public void onTopicChangeEvent(TopicChangeEvent event) {
    }

    public void onChannelKickEvent(ChannelKickEvent event) {
    }
}

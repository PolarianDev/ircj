package dev.polarian.ircj;

public enum UserMode {
    NONE(0),
    WALLOPS(4),
    INVISIBLE(8);
    final int bit;

    UserMode(int bit) {
        this.bit = bit;
    }

    public int getBit() {
        return this.bit;
    }
}
